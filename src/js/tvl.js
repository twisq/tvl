/**
 * Created by paul on 24/12/2016.
 */

// Constants
var path = 'img/default/';
var extension = '.png';
var validblocks = [
    'cross0',
    'cross45',
    'doublel0-r',
    'doublel90-r',
    'doubler0-r',
    'doubler90-r',
    'doublel0-b',
    'doublel90-b',
    'doubler0-b',
    'doubler90-b',
    'edge0',
    'edge90',
    'edge180',
    'edge270',
    'end0',
    'end135',
    'end180',
    'end225',
    'end270',
    'end315',
    'end45',
    'end90',
    'signal0-g',
    'signal135-g',
    'signal180-g',
    'signal225-g',
    'signal270-g',
    'signal315-g',
    'signal45-g',
    'signal90-g',
    'marker0',
    'marker135',
    'marker45',
    'marker90',
    'platform',
    'road0',
    'road45',
    'road90',
    'road135',
    'signal0-r',
    'signal135-r',
    'signal180-r',
    'signal225-r',
    'signal270-r',
    'signal315-r',
    'signal45-r',
    'signal90-r',
    'switchl0-r',
    'switchl135-r',
    'switchl180-r',
    'switchl225-r',
    'switchl270-r',
    'switchl315-r',
    'switchl45-r',
    'switchl90-r',
    'switchl0-b',
    'switchl135-b',
    'switchl180-b',
    'switchl225-b',
    'switchl270-b',
    'switchl315-b',
    'switchl45-b',
    'switchl90-b',
    'switchr0-r',
    'switchr0-b',
    'switchr135-r',
    'switchr180-r',
    'switchr225-r',
    'switchr270-r',
    'switchr315-r',
    'switchr45-r',
    'switchr90-r',
    'switchr135-b',
    'switchr180-b',
    'switchr225-b',
    'switchr270-b',
    'switchr315-b',
    'switchr45-b',
    'switchr90-b',
    'track0',
    'track135',
    'track45',
    'track90',
    'turnl0',
    'turnl180',
    'turnl270',
    'turnl90',
    'turnr0',
    'turnr180',
    'turnr270',
    'turnr90',
    'void',
    'water0',
    'water45',
    'water90',
    'water135'
];
var speed = 4;

// Helper objects
var voidobject = {name: 'void'};

// Variables
var size_y = 2;
var size_x = 2;
var mode='run';
var tick = 0;
var timer;
var nextTrainId = 1;
var score = 0;
var hour, minute, second;

// Data collections
var map = [];
var trains = [];

// Initialization
function init() {
    map = JSON.parse(scene);
    size_y = map.length;
    size_x = map[0].length;
    refreshMap();
    timer = setInterval(action, 500);
}

function initEmptyMap() {
    var i, j;

    for (i = 0; i < size_y; i++) {
        map[i] = [];
        for (j = 0; j < size_x; j++) {
            // Clone object.
            var block = JSON.parse(JSON.stringify(voidobject));
            map[i][j] = block;
        }
    }
    refreshMap();
}

// Show map
function refreshMap() {
    var s;

    s = "<table class='map'>";
    for (var i=0; i<size_y; i++) {
        s += "<tr class='map'>";
        for (j=0; j<size_x; j++) {
            var name = map[i][j].name;
            var filename = path + name + extension;
            var ana = analyseBlock(map[i][j]);

            var platform = false;
            if (ana.family == 'mar') {
                var id = map[i][j].id;
                if (typeof id != 'undefined') {
                    var separator_position = id.indexOf('-');
                    if (separator_position > -1) {
                        platform = id.substr(separator_position + 1);
                    }
                }
            }
            s += "<td class='map' onclick='clickOnBlock(this)'>";
            if (ana.family == 'voi') {
                if (map[i][j].id != undefined) {
                    s += "<span class='label'>" + map[i][j].id + "</span>";
                }
            }
            s += "<div class='cell'><img src='" + filename + "'/></div>";
            if (platform) {
                s += "<div class='platform'>" + platform + "</div>";
            }
            s += "</td>";
        }
        s += "</tr>";
    }

    document.getElementById("tableau").innerHTML = s;
}

// Read mode from form.
function getMode() {
    var radios = document.getElementsByName('mode');

    for (var i = 0, length = radios.length; i < length; i++) {
        if (radios[i].checked) {
            mode = radios[i].value;
            break;
        }
    }

    if (mode == 'run') {
        document.getElementById("edit_buttons").className = "hidden";
    } else {
        document.getElementById("edit_buttons").className = "not-hidden";
    }
}

// User has clicked on a block.
function clickOnBlock(td) {
    var element = td2img(td);
    switch(mode) {
        case 'run':
            runClick(element);
            break;
        case 'place':
            changeImage(element);
            break;
        case 'edit':
            editBlock(element);
            break;
        case 'insert-row':
            insertRow(element);
            break;
        case 'insert-column':
            insertColumn(element);
            break;
        case 'delete-row':
            deleteRow(element);
            break;
        case 'delete-column':
            deleteColumn(element);
            break;
        default:
            alert('invalid mode');
            break;
    }
}

function runClick(element) {

    var point = getPoint(element);
    var x = point.x;
    var y = point.y;

    // If there is a train on the clicked position. Give it some speed.
    if (element.className == 'train') {
        for (var i = 0; i < trains.length; i++) {
            if ((trains[i].x == x) && (trains[i].y == y)) {
                message("Train departing: " + trains[i].id);
                trains[i].speed = 1;
            }
        }
    }

    var ana = analyseBlock(map[y][x]);
    switch(ana.family) {
        case 'swi':
        case 'dou':
            if (ana.state=='-b') ana.state = '-r';
            else ana.state = '-b';
            map[y][x].name = ana2name(ana);
            element.src = path + map[y][x].name + extension;
            break;
        case 'sig':
            if (ana.state=='-r') {
                ana.state = '-g';
                if (map[y][x].waitingTrain != null) {
                    // If a train is waiting, give it some speed.
                    map[y][x].waitingTrain.speed = 1;
                }
            }
            else ana.state = '-r';
            map[y][x].name = ana2name(ana);
            element.src = path + map[y][x].name + extension;
            break;
        case 'edg':
            createTrain(element, ana.direction);
            break;
    }
}

function changeImage(element) {
    var point = getPoint(element);
    var x = point.x;
    var y = point.y;

    var newname = prompt("New block name:", map[y][x].name);
    if (newname != null) {
        var newblock = name2block(newname);
        if (validblocks.indexOf(newblock.name) > -1) {
            map[y][x] = newblock;
            element.src = path + newblock.name + extension;
        } else {
            alert("Invalid block: " + newname);
        }
    }
}

function editBlock(element) {
    var point = getPoint(element);
    var x = point.x;
    var y = point.y;

    var oldid = map[y][x].id;
    var newid = prompt("New id:", oldid);
    if (newid != null) {
        map[y][x].id = newid;
        refreshMap();
    }
}

function insertRow(element) {
    var point = getPoint(element);
    var x = point.x;
    var y = point.y;

    var newmap = [];
    var i,j,k;
    k=0;
    for (i=0; i<size_y; i++) {
        if (i==y) {
            newmap[k] = [];
            for (j=0; j<size_x; j++) {
                var block = JSON.parse(JSON.stringify(voidobject));
                newmap[k][j] = block;
            }
            k++;
        }
        newmap[k]=map[i];
        k++;
    }
    map=newmap;
    size_y++;
    refreshMap();
}

function insertColumn(element) {
    var point = getPoint(element);
    var x = point.x;
    var y = point.y;

    var newmap = [];
    var i,j,k;
    for (i=0; i<size_y; i++) {
        newmap[i] = [];
        k=0;
        for (j=0; j<size_x; j++) {
            if (j==x) {
                var block = JSON.parse(JSON.stringify(voidobject));
                newmap[i][k] = block;
                k++;
            }
            newmap[i][k] = map[i][j];
            k++;
        }
    }
    map=newmap;
    size_x++;
    refreshMap();
}

function deleteColumn(element) {
    if (size_x < 2) return;

    var point = getPoint(element);
    var x = point.x;
    var y = point.y;

    var newmap = [];
    var i,j,k;
    for (i=0; i<size_y; i++) {
        newmap[i] = [];
        k=0;
        for (j=0; j<size_x; j++) {
            if (j!=x) {
                newmap[i][k] = map[i][j];
                k++;
            }
        }
    }
    map=newmap;
    size_x--;
    refreshMap();
}

function deleteRow(element) {
    if (size_y < 2) return;

    var point = getPoint(element);
    var x = point.x;
    var y = point.y;

    var newmap = [];
    var i,j,k;
    k=0;
    for (i=0; i<size_y; i++) {
        if (i!=y) {
            newmap[k] = map[i];
            k++;
        }
    }
    map=newmap;
    size_y--;
    refreshMap();
}

function showSource() {
    document.getElementById("source").innerHTML = JSON.stringify(map);
}

function loadSource() {
    var json = prompt("Enter source in JSON here.");
    map = JSON.parse(json);
    size_y = map.length;
    size_x = map[0].length;
    refreshMap();
}

function setSize() {
    if (confirm("This will delete whole tableau. Are you sure?")) {
        var x = prompt("Enter horizontal size here.");
        var y = prompt("Enter vertical size here.");
        if (x!=null && y!=null) {
            size_x = x;
            size_y = y;
            initEmptyMap();
        }
    }
}

function createTrain(element, corner) {
    var point = getPoint(element);
    var x = point.x;
    var y = point.y;

    var newtrain = {id: nextTrainId++, x: x, y: y, direction: corner, speed: 1};
    trains.push(newtrain);

    element.className = 'train';
}

// Timer functions
function updateTime() {
    var oldmin = minute;
    hour = 12 + (tick * speed) / 3600>>0;
    minute = ((tick * speed) % 3600) / 60>>0;
    second = (tick * speed) % 60;
    document.getElementById('time').innerHTML = hour + ':' + pad(minute,2) + ':' + pad(second,2);
    return oldmin != minute;
}

function updateTrainList() {
/*    var html = '<ul>';
    for (var i=0; i<trains.length; i++) {
        html += '<li>' + trains[i].id;
        var schedule = trains[i].schedule;
        if (schedule) {
            html += ' ' + schedule.from;
            for (var j=0; j<schedule.to.length; j++) {
                html += ',' + schedule.to[j];
            }
        }
        html += '</li>';
    }
    html += '</ul>';*/
    var html = "<table class='schedule'>";
    html += "<th class='schedule'>nr.</th><th class='schedule'>van</th><th class='schedule'>naar</th><th class='schedule'>via</th>";
    for (var i=0; i<trains.length; i++) {
        html += '<tr class="schedule">';
        html += '<td class="schedule">' + trains[i].id + '</td>';
        var schedule = trains[i].schedule;
        if (schedule) {
            html += '<td class="schedule">' + schedule.first + '</td>';
            html += '<td class="schedule">' + schedule.last + '</td>';
            html += '<td class="schedule">';
            html += ' ' + schedule.from;
            for (var j = 0; j < schedule.to.length; j++) {
                html += ',' + schedule.to[j];
            }
        }
        html += '</td>';
        html += '</tr>';
    }
    html += "</table>";
    document.getElementById('source').innerHTML = html;
}

function startTrain(schedule) {
    // Find start point
    for (var y=0; y<size_y; y++) {
        for (var x=0; x<size_x; x++) {
            if (map[y][x].id == schedule.from) {
                // Start train at this position.
                var ana = analyseBlock(map[y][x]);
                var trainnr = schedule.line + nextTrainId++ % 100;
                var newtrain = {id: trainnr, x: x, y: y, direction: ana.direction, speed: 1, schedule: schedule};
                trains.push(newtrain);

                var img = getTableauImage(x,y);
                img.className = 'train';
            }
        }
    }
    updateTrainList();
}

function checkForNewTrains() {
    for (var i=0; i<schedule.length; i++) {
        if (schedule[i].minute == minute) {
            startTrain(schedule[i]);
        }
    }
}

function action() {
    if (mode == 'run') {
        tick++;
        document.getElementById('ticks').innerHTML = tick;
        var newminute = updateTime();
        var i = 0;
        while (i < trains.length) {
            if (moveTrain(trains[i])) i++;
            else {
                trains.splice(i, 1);
                updateTrainList();
            }
        }
        if (newminute) checkForNewTrains();
    }
}

function moveTrain(train) {
    // Do not move a stopped train.
    if (train.speed == 0) return true;

    // Find next position.
    var x = train.x;
    var y = train.y;
    switch(train.direction) {
        case 0:
            x++;
            break;
        case 45:
            x++;
            y--;
            break;
        case 90:
            y--;
            break;
        case 135:
            x--;
            y--;
            break;
        case 180:
            x--;
            break;
        case 225:
            x--;
            y++;
            break;
        case 270:
            y++;
            break;
        case 315:
            x++;
            y++;
            break;
    }

    // Check if train must stop.
    var next = analyseBlock(map[y][x]);
    switch (next.family) {
        case 'end':
            train.speed = 0;
            train.direction = (train.direction + 180) % 360;
            return true;
        case 'sig':
            if ((next.state == '-r') && (train.direction == next.direction)) {
                train.speed = 0;
                map[y][x].waitingTrain = train;
                return true;
            }
            break;
        default:
            break;
    }

    // Remove train from old position.
    var img = getTableauImage(train.x, train.y);
    img.className = '';

    // Check if train has to disappear.
    if (train.direction == -1) return false;

    if ((x >= size_x) || (y >= size_y) || (x < 0) || (y < 0)) {
        error(train);
        return false;
    }

    // Move train.
    train.x = x;
    train.y = y;

    img = getTableauImage(train.x, train.y);
    if (img.className == 'train') {
        // Two trains on same position. BOOM!
        error(train);
        message("Botsing!");
        return false;
    }
    img.className = 'train';
    updateDirection(train);
    return true;
}

// Check if train is at one of its scheduled destinations.
function checkDestination(train) {
    var currentId = map[train.y][train.x].id;
    if (train.schedule) {
        for (var i=0; i<train.schedule.to.length; i++) {
            if (currentId == train.schedule.to[i]) return true;
        }
    }
    return false;
}

// return false if train has to disappear.
function updateDirection(train) {
    var ana = analyseBlock(map[train.y][train.x]);
    var dif = train.direction - ana.direction;
    switch(ana.family) {
        case 'edg':
            // Signal, train has to disappear.
            if (checkDestination(train)) addScore(10);
            train.direction = -1;
            return false;
        case 'mar':
            if ((dif != 0) && (dif != 180) && (dif != -180)) {
                error(train);
                return false;
            }
            if (checkDestination(train)) {
                addScore(1);
                train.speed = 0;
            }
            break;
        case 'tra':
        case 'sig':
        case 'wat':
        case 'roa':
            if ((dif != 0) && (dif != 180) && (dif != -180)) {
                message("Ontsporing!");
                error(train);
                return false;
            }
            break;
        case 'tur':
            if (!updateTurn(train, ana)) {
                message("Ontsporing!");
                error(train);
                return false;
            }
            break;
        case 'swi':
            if (ana.state == '-r') {
                if ((dif != 0) && (dif != 180) && (dif != -180)) {
                    message("Ontsporing!");
                    error(train);
                    return false;
                }
            } else {
                if (!updateTurn(train, ana)) {
                    message("Ontsporing!");
                    error(train);
                    return false;
                }
            }
            break;
        case 'dou':
            if (!updateDouble(train, ana)) {
                message("Ontsporing!");
                error(train);
                return false;
            }
            break;
        case 'cro':
            var dir = train.direction;
            if (ana.direction == 45) dir -= 45;
            if ((dir % 90) != 0) {
                message("Ontsporing!");
                error(train);
                return false;
            }
            break;
        default:
            message("Ontsporing!");
            error(train);
            return false;
    }
    return true;
}

function updateDouble(train, ana) {
    if (ana.direction == 0) {
        // Direction 0
        if (ana.lr == 'l') {
            // Left
            if ((train.direction == 90) || (train.direction == 135) || (train.direction == 270) || train.direction == 315) return false;
            if (ana.state == '-b') {
                switch (train.direction) {
                    case 0:
                        train.direction = 45;
                        break;
                    case 45:
                        train.direction = 0;
                        break;
                    case 180:
                        train.direction = 225;
                        break;
                    case 225:
                        train.direction = 180;
                        break;
                }
            }
        } else {
            // Right
            if ((train.direction == 45) || (train.direction == 90) || (train.direction == 225) || train.direction == 270) return false;
            if (ana.state == '-b') {
                switch (train.direction) {
                    case 0:
                        train.direction = 315;
                        break;
                    case 315:
                        train.direction = 0;
                        break;
                    case 180:
                        train.direction = 135;
                        break;
                    case 135:
                        train.direction = 180;
                        break;
                }
            }
        }
    } else {
        // Direction 90
        if (ana.lr == 'l') {
            // Left
            if ((train.direction == 0) || (train.direction == 45) || (train.direction == 180) || train.direction == 225) return false;
            if (ana.state == '-b') {
                switch (train.direction) {
                    case 90:
                        train.direction = 135;
                        break;
                    case 135:
                        train.direction = 90;
                        break;
                    case 270:
                        train.direction = 315;
                        break;
                    case 315:
                        train.direction = 270;
                        break;
                }
            }
        } else {
            // Right
            if ((train.direction == 0) || (train.direction == 135) || (train.direction == 180) || train.direction == 315) return false;
            if (ana.state == '-b') {
                switch (train.direction) {
                    case 90:
                        train.direction = 45;
                        break;
                    case 45:
                        train.direction = 90;
                        break;
                    case 270:
                        train.direction = 225;
                        break;
                    case 225:
                        train.direction = 270;
                        break;
                }
            }
        }
    }
    return true;
}

function updateTurn(train, ana) {
    if (ana.lr == 'l') {
        switch (ana.direction) {
            case 0:
                if (train.direction == 0) train.direction = 45;
                else if (train.direction == 225) train.direction = 180;
                else return false;
                break;
            case 45:
                if (train.direction == 45) train.direction = 90;
                else if (train.direction == 270) train.direction = 225;
                else return false;
                break;
            case 90:
                if (train.direction == 90) train.direction = 135;
                else if (train.direction == 315) train.direction = 270;
                else return false;
                break;
            case 135:
                if (train.direction == 135) train.direction = 180;
                else if (train.direction == 0) train.direction = 315;
                else return false;
                break;
            case 180:
                if (train.direction == 180) train.direction = 225;
                else if (train.direction == 45) train.direction = 0;
                else return false;
                break;
            case 225:
                if (train.direction == 225) train.direction = 270;
                else if (train.direction == 90) train.direction = 45;
                else return false;
                break;
            case 270:
                if (train.direction == 270) train.direction = 315;
                else if (train.direction == 135) train.direction = 90;
                else return false;
                break;
            case 315:
                if (train.direction == 315) train.direction = 0;
                else if (train.direction == 180) train.direction = 135;
                else return false;
                break;
        }
    } else {
        switch (ana.direction) {
            case 0:
                if (train.direction == 0) train.direction = 315;
                else if (train.direction == 135) train.direction = 180;
                else return false;
                break;
            case 45:
                if (train.direction == 45) train.direction = 0;
                else if (train.direction == 180) train.direction = 225;
                else return false;
                break;
            case 90:
                if (train.direction == 90) train.direction = 45;
                else if (train.direction == 225) train.direction = 270;
                else return false;
                break;
            case 135:
                if (train.direction == 135) train.direction = 90;
                else if (train.direction == 270) train.direction = 315;
                else return false;
                break;
            case 180:
                if (train.direction == 180) train.direction = 135;
                else if (train.direction == 315) train.direction = 0;
                else return false;
                break;
            case 225:
                if (train.direction == 225) train.direction = 180;
                else if (train.direction == 0) train.direction = 45;
                else return false;
                break;
            case 270:
                if (train.direction == 270) train.direction = 225;
                else if (train.direction == 45) train.direction = 90;
                else return false;
                break;
            case 315:
                if (train.direction == 315) train.direction = 270;
                else if (train.direction == 90) train.direction = 135;
                else return false;
                break;
        }
    }
    return true;
}

// Helper functions
function analyseBlock(block) {
    var re = /([a-z]{3})([a-z]*)([0-9]*)(-[rgb])?/ig;
    var result = re.exec(block.name);
    if (result == null) {
      var analysis = {
          family: '',
          name: '',
          lr: '',
          direction: 0,
          state: undefined
      }
    } else {
        var analysis = {
            family: result[1],
            name: result[1] + result[2],
            lr: result[2][result[2].length - 1],
            direction: parseInt(result[3]),
            state: result[4]};
    }
    return analysis;
}

function getPoint(element) {
    var td = element.parentElement.parentElement;
    var tr = td.parentElement;
    var y = tr.rowIndex;
    var x = td.cellIndex;
    return {x: x, y:y};
}

function pad(x, p) {
    var s = "" + x;
    while (s.length < p) s = "0" + s;
    return s;
}

function td2img(td) {
    var div = td.getElementsByClassName('cell')[0];
    var img = div.getElementsByTagName('img')[0];
    return img;
}

function getTableauImage(x, y) {
    var tableau = document.getElementById('tableau');
    var table = tableau.getElementsByTagName('table')[0];
    var row = table.getElementsByTagName('tr')[y];
    var td = row.getElementsByTagName('td')[x];
    return td2img(td);
}

function ana2name(analysis) {
    return analysis.name + analysis.direction + analysis.state;
}

function name2block(name) {
    var newblock = {name: name};
    if (name[0] < 'a') {
        switch (name[0]) {
            case 'T':
                newblock.name = 'track' + name.substr(1);
                newblock.type = 'track';
                break;
            case 'B':
                newblock.name = 'turn' + name.substr(1);
                newblock.type = 'turn';
                break;
            case 'W':
                newblock.name = 'switch' + name.substr(1);
                newblock.type = 'switch';
                break;
            case 'D':
                newblock.name = 'double' + name.substr(1);
                newblock.type = 'double';
                break;
            case 'S':
                newblock.name = 'signal' + name.substr(1) + '-r';
                newblock.type = 'signal';
                break;
        }
    }
    return newblock;
}

function addScore(x) {
    score += x;
    document.getElementById('score').innerHTML = score;
}

function message(msg) {
    document.getElementById('message').innerHTML = msg;
}

// Something terrible has happened.
function error(train) {
    train.speed = 0;
    var img = getTableauImage(train.x, train.y);
    img.className = 'error';
    clearInterval(timer);
}
